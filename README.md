# copper-processing
[![OntoFlow](https://gitlab.com/kupferdigital/process-graphs/copper-processing/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=OntoFlow&key_width=80)](https://kupferdigital.gitlab.io/process-graphs/copper-processing)
[![Graph](https://gitlab.com/kupferdigital/process-graphs/copper-processing/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=Turtle&key_width=80)](https://kupferdigital.gitlab.io/process-graphs/copper-processing/index.ttl)

A knowledge graph representing the general processing of copper from ore or recyclate to copper product by HZDR HIF.

<img src="https://kupferdigital.gitlab.io/process-graphs/copper-processing/copper-processing.svg">
